﻿using GrpcServer.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace GrpcServer.Services
{
    public class MedicationService
    {
        public MedicationService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        public IWebHostEnvironment WebHostEnvironment { get; }

        public IEnumerable<MedicationItem> GetMedicationItems(int id)
        {
            var path = id == 1 ? @"C:\Users\rault\source\repos\GrpcAssignment\GrpcServer\data\document.json" : @"C:\Users\rault\source\repos\GrpcAssignment\GrpcServer\data\document1.json";
            using (var jsonFileReader = File.OpenText(path))
            {
                return JsonSerializer.Deserialize<MedicationItem[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}
