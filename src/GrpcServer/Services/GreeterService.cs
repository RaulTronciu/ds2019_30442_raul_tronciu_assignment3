using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcServer.Services;
using Microsoft.Extensions.Logging;

namespace GrpcServer
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        private readonly MedicationService medicationService;

        public GreeterService(ILogger<GreeterService> logger, MedicationService medicationService)
        {
            _logger = logger;
            this.medicationService = medicationService;
        }

        public override async Task GetMedicalPlan(NewMedicalPlanRequest request, IServerStreamWriter<MedicalItem> responseStream, ServerCallContext context)
        {
            List<MedicalItem> medicalItems = new List<MedicalItem>();

            var list = medicationService.GetMedicationItems(request.Id);

            foreach(var el in list)
            {
                medicalItems.Add(new MedicalItem
                {
                    Name = el.Name,
                    Quantity = el.Quantity,
                    IntervalStart = el.IntervalStart,
                    IntervalStop = el.IntervalStop,
                    Observation = el.Observation
                });
            }

            foreach (var item in medicalItems)
            {
                await responseStream.WriteAsync(item);
            }
        }
    }
}
