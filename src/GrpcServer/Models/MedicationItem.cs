﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace GrpcServer.Models
{
    public class MedicationItem
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("quantity")]
        public string Quantity { get; set; }
        [JsonPropertyName("intervalStart")]
        public string IntervalStart { get; set; }
        [JsonPropertyName("intervalStop")]
        public string IntervalStop { get; set; }
        [JsonPropertyName("observation")]
        public string Observation { get; set; }

        public override string ToString() => JsonSerializer.Serialize<MedicationItem>(this);
    }
}
