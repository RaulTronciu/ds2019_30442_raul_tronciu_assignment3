﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrpcClient.Models;
using GrpcClient.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace GrpcClient.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly GreetService _medication;
        public IList<MedicationItem> Medication = new List<MedicationItem>();

        public IndexModel(ILogger<IndexModel> logger,
            GreetService medication)
        {
            _logger = logger;
            _medication = medication;
        }

        public async Task OnGetAsync()
        {
            var list = await _medication.GetMedicalItems();

            foreach(var item in list)
            {
                Medication.Add(new MedicationItem 
                {
                    Name = item.Name,
                    Quantity = item.Quantity,
                    IntervalStart = item.IntervalStart,
                    IntervalStop = item.IntervalStop,
                    Observation = item.Observation
                });
            }
        }
    }
}
