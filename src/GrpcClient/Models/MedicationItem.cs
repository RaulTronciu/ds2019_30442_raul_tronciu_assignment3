﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace GrpcClient.Models
{
    public class MedicationItem
    {
        public string Name { get; set; }
        public string Quantity { get; set; }
        public string IntervalStart { get; set; }
        public string IntervalStop { get; set; }
        public string Observation { get; set; }
    }
}
