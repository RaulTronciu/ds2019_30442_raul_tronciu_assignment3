﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcServer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GrpcClient.Services
{
    public class GreetService
    {
        public async Task<List<MedicalItem>> GetMedicalItems()
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var grpcClient = new Greeter.GreeterClient(channel);

            var request = new NewMedicalPlanRequest
            {
                Id = 1
            };
            var medicalList = new List<MedicalItem>();

            using (var call = grpcClient.GetMedicalPlan(request))
            {
                while (await call.ResponseStream.MoveNext())
                {
                    var currentItem = call.ResponseStream.Current;
                    medicalList.Add(currentItem);
                }
            }

            return medicalList;
        }
    }
}
