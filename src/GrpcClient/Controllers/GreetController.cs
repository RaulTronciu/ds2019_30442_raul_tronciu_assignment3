﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrpcClient.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrpcClient.Controllers
{
    public class GreetController : Controller
    {
        private readonly GreetService _service;

        public GreetController(GreetService service)
        {
            _service = service;
        }

        [HttpGet("get")]
        public ActionResult Get()
        {
            try
            {
                var list = _service.GetMedicalItems();
                return Ok(list);
            }
            catch
            {
                return View();
            }
        }
    }
}